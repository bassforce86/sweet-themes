#!/bin/bash
# Pull down all the themes into the Downloads folder.

themes=(mars nova Ambar Ambar-Blue)

cd $HOME/Downloads

if [[ ! -d "sweet-themes" ]]; then
	mkdir sweet-themes
fi

cd sweet-themes
rm -rf ./*

echo "# cloning themes... "
# Grab the master branch for the default theme.
git clone https://github.com/EliverLara/Sweet.git ./Sweet -q



for theme in ${themes[@]}; do
	mkdir Sweet_$theme;
	cp -ra ./Sweet/. Sweet_$theme/;
	cd ./Sweet_$theme;
	git checkout $theme;
	cd ../
done


echo "# moving files..."
echo "from: ${PWD} \nto: /usr/share/themes"
cd ../
sudo mv ./sweet-themes/* /usr/share/themes/

echo "-- installation complete --"
