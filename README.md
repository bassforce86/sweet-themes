# Sweet Themes Installer (Debian/Ubuntu)

Sweet themes install script for use with Gnome Tweak tool

# Credits
All credit goes to [EliverLara](https://github.com/EliverLara) for their work, these are their themes, not mine. I simply wanted a lazy install script for them.

Please head on over to [https://github.com/EliverLara/Sweet](https://github.com/EliverLara/Sweet) to see their work and install using their recommended methods if this doesn't work for you.

### Disclaimer

**This script uses sudo to copy the themes over to `/usr/share/themes` for use globally.**

This was written just for my own personal use. Feel free to suggest fixes but I may not get around to updating it! 

